import { RepositoryFactory as Repo} from "@/repository/RepositoryFactory";
const PR = Repo.get('request');
import router from '@/router'

const state = {
    user: {},
    token: null,
}

const getters = {
    user(state){
        return state.user
    },
    token(state) {
        return state.token
    }
}

const mutations = {
    user(state,data){
        state.user = data.user;
        state.token = data.access_token ? data.access_token : state.token
    },

    logout(state) {
        state.token = null;
        state.user = {};
    }
}

const actions = {

    async register({ name, email, password, phone, device }) {
        let payload = { name, email, password, phone, device }
        return await PR.register(payload)
    },

    async login({ commit }, form) {
        const res = await PR.login(form);
        commit('user',res);
        return res;
    },

    async logout({commit}){
        const res  = await commit('logout');
        router.push('/login')
        return res;
    },

    async forgotpassword({ email }) {
        const res = await PR.forgotpassword(email);
        return res
    }
}

export default {
    namespaced:true,
    state,
    getters,
    mutations,
    actions
}