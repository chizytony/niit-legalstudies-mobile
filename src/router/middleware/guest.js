export default function guest ({ next, store }){
    if(store.state.user.token){
        return next({
            path: '/account/dashboard'
        });
    }

    return next()
}