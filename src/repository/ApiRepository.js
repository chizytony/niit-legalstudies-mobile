import Vue from 'vue'
import axios from "axios"
import store from "@/store"

axios.defaults.withCredentials = false
axios.interceptors.response.use(response => response.data)
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
axios.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    if (!error.response) {
        Vue.$toast.error('Network Error, Please check your network connection!')
    }
    if (error.response.status === 401 || error.response.data.message == 'Token is Expired') {
        store.dispatch('user/logout')
    }
    return Promise.reject(error);
});
//Default setup for Authorization
axios.interceptors.request.use(function (config) {
    const token = store.state.auth.token
    config.headers.Authorization =  `Bearer ${token}`;
    config.headers['dev-auth'] = process.env.VUE_APP_ENV_ACCESS_GRANT
    return config;
});

let url = process.env.VUE_APP_ENV_STAGING;
//if(process.env.NODE_ENV === 'development' || 'staging'){
  //  url = process.env.VUE_APP_ENV_STAGING
//} else {
  //  url = process.env.VUE_APP_ENV_PRODUCTION
//}


export default {
   register(payload) {
      return axios.post(url + '/register', payload)
   }
}