import auth from '@/router/middleware/auth'

const protectedRoutes = [
   {
      path: '/account',
      component: () => import('@/layouts/authenticated'),
      children: [
         {
            path: '',
            redirect: '/account/dashboard'
         },
         {
            path: '/account/dashboard',
            component: () => import('@/views/protected/dashboard'),
            meta: {
               middleware: [ auth ]
            }  
         },
         {
            path: '/account/notifications',
            component: () => import('@/views/protected/notifications'),
            meta: {
               middleware: [ auth ]
            }
         },
         {
            path: '/account/ibooks',
            component: () => import('@/views/protected/ibooks'),
            meta: {
               middleware: [ auth ]
            }
         },
         {
            path: '/account/carts',
            component: () => import('@/views/protected/carts')
         }
      ]
   }
]

export default protectedRoutes;