import { createApp } from 'vue'
import App from './App'
import router from './router';
import { IonicVue } from '@ionic/vue'
import store from './store'

/* Core CSS required for Ionic components to work properly */
import '@ionic/vue/css/core.css'

/* Basic CSS for apps built with Ionic */
import '@ionic/vue/css/normalize.css'
import '@ionic/vue/css/structure.css'
import '@ionic/vue/css/typography.css'

/* Theme variables */
import 'tailwindcss/tailwind.css'
import './theme/variables.css'
import './theme/onegeco.css'

/* Global Component */
import AuthenticatedLayout from './layouts/authenticated.vue'

const app = createApp(App)
  .use(IonicVue)
  .use(store)
  .use(router)
  .component('authenticated-layout', AuthenticatedLayout)

router.isReady().then(() => {
  app.mount('#app');
});