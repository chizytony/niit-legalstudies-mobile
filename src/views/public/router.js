import guest from '@/router/middleware/guest'

const publicRoutes = [
   {
      path: '/',
      redirect: '/'
   },
   {
      path: '/login',
      name: 'Login',
      component: () => import('@/views/public/login'),
      meta: {
         middleware: [ guest ],
      }
   },
   {
      path: '/register',
      name: 'Register',
      component: () => import('@/views/public/register'),
      meta: {
         middleware: [ guest ]
      }
   }
]
 
export default publicRoutes;